import React, { useState } from "react";

import AddIcon from "@material-ui/icons/Add";
import Alert from "@material-ui/lab/Alert";
import AppBar from "@material-ui/core/AppBar";
import Fab from "@material-ui/core/Fab";
import Grid from "@material-ui/core/Grid";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import WeatherCard from "./weatherCard";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
        display: "flex",
        flexDirection: "column",
        height: "100vh",
        overflow: "hidden",
    },
    containerGrid: {
        flex: 1,
        overflowY: "auto",
        padding: "2em",
    },
    addButton: {
        position: "absolute",
        margin: "1em",
        right: 0,
        bottom: 0,
    },
    appBar: {
        backgroundColor: "#4051b5",
    },
    logo: {
        fontFamily: "Nunito",
    },
}));

const LOCAL_STORAGE_KEY = "locations";
function saveToLocalStorage(locations: any) {
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(locations));
}

function readFromLocalStorage() {
    const storedLocations = localStorage.getItem(LOCAL_STORAGE_KEY);
    return storedLocations ? JSON.parse(storedLocations) : [];
}

function App() {
    const classes = useStyles();
    const [weatherLocations, setWeatherLocations] = React.useState(readFromLocalStorage());
    const [addAlert, setAddAlert] = useState(false);
    const [removeAlert, setRemoveAlert] = useState(false);

    const handleAddClick = () => {
        setWeatherLocations([...weatherLocations, ""]);
        handleShowAddAlert();
    };

    const handleShowAddAlert = () => {
        setAddAlert(true);

        setTimeout(() => {
            setAddAlert(false);
        }, 4000);
    };

    const handleShowRemoveAlert = () => {
        setRemoveAlert(true);

        setTimeout(() => {
            setRemoveAlert(false);
        }, 4000);
    };

    const updateLocations = locations => {
        setWeatherLocations(locations);
        saveToLocalStorage(locations);
    };

    const removeAtIndex = index => () => {
        updateLocations(weatherLocations.filter((_, locationIndex) => locationIndex !== index));
        handleShowRemoveAlert();
    };

    const updateAtIndex = index => updatedLocation =>
        updateLocations(
            weatherLocations.map((location, locationIndex) => (locationIndex === index ? updatedLocation : location)),
        );

    const canAddOrRemove = React.useMemo(() => weatherLocations.every(location => location !== ""), [weatherLocations]);

    return (
        <div className={classes.root}>
            <AppBar className={classes.appBar} position="static">
                <Toolbar>
                    <Typography className={classes.logo} variant="h5" color="inherit">
                        7 day Forecast ☀️
                    </Typography>
                </Toolbar>
            </AppBar>
            <Grid container spacing={3} className={classes.containerGrid}>
                {weatherLocations.map((location, index) => (
                    <Grid key={location} xs={12} sm={6} md={4} lg={3} item>
                        <WeatherCard
                            location={location}
                            canDelete={!location || canAddOrRemove}
                            onDelete={removeAtIndex(index)}
                            onUpdate={updateAtIndex(index)}
                        />
                    </Grid>
                ))}
            </Grid>
            <Fab
                onClick={handleAddClick}
                aria-label="add weather location"
                className={classes.addButton}
                color="primary"
                disabled={!canAddOrRemove}
            >
                <AddIcon />
            </Fab>
            {addAlert && (
                <>
                    {" "}
                    <Alert severity="info">Please choose a location</Alert>{" "}
                </>
            )}
            {removeAlert && (
                <>
                    {" "}
                    <Alert severity="success">You just removed a location</Alert>{" "}
                </>
            )}
        </div>
    );
}

export default App;
