import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import LocationEntry from "./locationEntry";
import LocationWeather from "./locationWeather";
import PropTypes from "prop-types";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
    root: {
        display: "flex",
        flexDirection: "column",
        borderRadius: 16,
    },
    content: { flex: 1 },
    remove: {
        color: "darkRed",
        opacity: 0.7,
    },
    actions: {
        padding: 4,
        alignItems: "right",
        justifyContent: "flex-end",
    },
    label: {
        justifyContent: "flex-end",
        "&:hover": {
            background: "white",
        },
    },
    textSizeSmall: {
        padding: 0,
    },
}));

function WeatherCard({ location, canDelete, onDelete, onUpdate }) {
    const classes = useStyles();

    return (
        <Card className={classes.root} variant="outlined">
            <CardContent className={classes.content}>
                {!location && <LocationEntry onUpdate={onUpdate} />}
                {location && <LocationWeather location={location} />}
            </CardContent>
            <CardActions className={classes.actions}>
                <Button
                    classes={{ label: classes.label, textSizeSmall: classes.textSizeSmall }}
                    disabled={!canDelete}
                    onClick={onDelete}
                    size="small"
                    color="primary"
                >
                    <HighlightOffIcon className={classes.remove} />
                </Button>
            </CardActions>
        </Card>
    );
}

WeatherCard.propTypes = {
    location: PropTypes.string.isRequired,
    canDelete: PropTypes.bool.isRequired,
    onDelete: PropTypes.func.isRequired,
    onUpdate: PropTypes.func.isRequired,
};

export default WeatherCard;
